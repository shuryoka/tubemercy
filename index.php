<?php
require_once 'bootstrap.php';

$videoList = $videoStore->findBy(['views', '!=', null], ['created_at' => 'asc']);
$video = $videoList[rand(0, count($videoList) - 1)];
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>TubeMercy</title>
    <meta name="description" content="This is a clone of petittube.com and is meant as tutorial on how to implement it.">
    <meta name="author" content="Christoph Frenes">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body style="background-color: black; color: #FFFFFF;">

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
    <div class="row">
        <div class="one-half column" style="margin-top: 50px;">
            <h1 class="hero">TubeMercy</h1>
            <p>TubeMercy features short YouTube videos with just a few or even zero views. Let's see what's out there.</p>
            <p><a href="https://christoph-frenes.de/tubemercy/" class="button button-primary">Next Video</a></p>
            <p>
                <iframe loading="lazy" width="560" height="315" src="https://www.youtube-nocookie.com/embed/<?php echo $video['video_id'] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </p>
            <p>If you are interested in how this works, stay tuned for the upcoming blog post and the code hosted at github.com.</p>
            <p>Credits: This site is inspired by <a href="https://petittube.com">petittube.com</a>. They were first.</p>
            <p>See: <a href="https://www.youtube.com/static?template=terms">Youtube Terms of Service</a> and <a href="https://policies.google.com/privacy">Google privacy policy</a></p>
        </div>
    </div>
</div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>

