# TubeMercy

This is a just for fun project which aims to rebuild [https://petittube.com](https://petittube.com).

**You can find the result here:**  
[https://christoph-frenes.de/tubemercy/](https://christoph-frenes.de/tubemercy/)

There's also a draft for a blog post about this:  
[blog post](blogpost.md)