<?php
require_once "vendor/autoload.php";

// set default date and time
date_default_timezone_set('UTC');
$dateTime = new DateTime();

// set logger
$logger = new \Monolog\Logger('general');
$logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/var/log/general.log'));

// set database
$databaseDir = __DIR__ . '/database';
$configuration = []; // use default config: https://sleekdb.github.io/#/configurations
$videoStore = new \SleekDB\Store("videos", $databaseDir, $configuration);

// google API
$apiKey = 'REPLACE_WITH_YOUR_API_KEY';
