<?php

// load bootstrap
require_once __DIR__ . "/bootstrap.php";

$logger->info('starting fetching');

$maxTimeSpanForRequest = 30; // in minutes
$time = time();
$startAt = trim(date("Y-m-d\TH:i:s\Z", $time - ($maxTimeSpanForRequest * 60))); // now (in RFC 3339) minus $maxTimeSpanForRequest minutes
$stopAt = trim(date("Y-m-d\TH:i:s\Z", $time)); // now (in RFC 3339)
$sortBy = 'date';
$maxResults = 50;

$url = "https://www.googleapis.com/youtube/v3/search?key=$apiKey&maxResults=$maxResults&order=$sortBy&publishedAfter=$startAt&publishedBefore=$stopAt&type=video&videoDuration=short&videoEmbeddable=true&eventType=completed&safeSearch=strict";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = json_decode(curl_exec($ch));
curl_close($ch);

if (!empty($response->error)) {
    $logger->error('API error - stopping execution');
    exit(1);
}

if ($response->pageInfo->resultsPerPage > $maxResults) {
    $logger->warning("to many results for given time spam. ignoring the rest.");
}

foreach ($response->items as $item) {
    if (!empty($item->id->videoId)) {
        $video = [
            "video_id" => $item->id->videoId,
            "title" => null,
            "views" => null,
            "created_at" => $dateTime->format("Y-m-d H:i:s"),
            "updated_at" => $dateTime->format("Y-m-d H:i:s")
        ];

        $find = $videoStore->findBy(['video_id', '=', $item->id->videoId]);

        if (empty($find)) {
            $videoStore->insert($video);
        }
    }
}

$logger->info('finished fetching');
