<?php

// load composer stuff
require_once "bootstrap.php";

$logger->info('starting updating');

// delete old videos
$dateTime->sub(new DateInterval('P2M')); // 2 months
//$dateTime->sub(new DateInterval('PT1M')); // 1 minute | just for debugging

try {
    // delete entries older than 2 month to keep database small
    $videoStore->deleteBy(['created_at', '<=', $dateTime]);
} catch (\SleekDB\Exceptions\IOException $e) {
} catch (\SleekDB\Exceptions\InvalidArgumentException $e) {
    $logger->error($e->getMessage());
}

// limit for number of views a video can have to stay in the list
$viewCountLimit = 15;

// duration in seconds a video can have to be allowed to stay in the list
$lengthLimit = 180; // 3 minutes

// bulk size for list requests
$videoBulkLimit = 50;

// bulk variables
$videoBulkList = [];
$videoCounter = 0;
$bulkCounter = 0;

// list of really found videos via the API
$foundVideoList = [];

// get list of videos from database
try {
    $videoList = $videoStore->findAll(['updated_at' => 'asc', '_id' => 'asc']);
} catch (\SleekDB\Exceptions\IOException $e) {
} catch (\SleekDB\Exceptions\InvalidArgumentException $e) {
    $logger->error($e->getMessage());
    exit(1);
}

// build bulks
foreach ($videoList AS $video) {
    $videoCounter++;

    $videoBulkList[$bulkCounter][] = $video['video_id'];

    // start new bulk
    if (!($videoCounter % $videoBulkLimit)) {
        $bulkCounter++;
    }
}

// create bulks of videos to reduce requests and make use of list requests
foreach ($videoBulkList AS $bulk) {
    $bulkAsString = implode(',', $bulk);

    $url = "https://www.googleapis.com/youtube/v3/videos?id=$bulkAsString&part=contentDetails,statistics,status&key=$apiKey";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = json_decode(curl_exec($ch));
    curl_close($ch);

    if (!empty($response->error)) {
        $logger->error('API error - stopping execution');
        exit(1);
    }

    foreach ($response->items AS $item) {
        // get id of current video in the database
        $videoId = $videoStore->findBy(['video_id', '=', $item->id]);
        $foundVideoList[] = $item->id;

        if ($item->status->privacyStatus != 'public') {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        if ($item->status->license != 'youtube') {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        if ($item->status->embeddable != true) {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        if ($item->status->madeForKids == true) {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        // delete from database if viewCount is higher than limit
        // if it's deleted it will not be tried to update it again and will be no longer a burden to the API request limit
        if ((int)$item->statistics->viewCount > $viewCountLimit) {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        // if we cannot fetch a view count that indicates the video is either private or was deleted
        if (empty($item->statistics->viewCount)) {
            $videoStore->deleteBy(['video_id', '=', $item->id]);
            continue;
        }

        // update database
        $videoStore->updateById($videoId[0]['_id'], ['views' => $item->statistics->viewCount, 'updated_at' => $dateTime->format("Y-m-d H:i:s")]);
    }
}

// diff the database videos and the API videos
foreach ($videoList AS $video) {
    if (!in_array($video['video_id'], $foundVideoList)) {
        // delete database entry if API did not found the video
        $videoStore->deleteBy(['video_id', '=', $video['video_id']]);
    }
}

$logger->info('finished updating');
