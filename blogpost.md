# TubeMercy, a HowTo

## Journey to rebuild petittube.com

The idea of [petittube,com](https://petittube.com) is to show short videos hosted on youtube.com with just a small number of views down to zero. In many cases these videos are in fact just tests or undeleted stream accidents. But of course there might be also content that could be interesting and deserves more views.

The page makes use of the embedding feature of YouTube. Every page hit or refresh loads a new random video. In theory you'll never get the same video twice and every video has a view count between 0 and roundabout 15.

The concept seems interesting and so I started to wonder if I could write that too. :thinking:

**Spoiler:** I did it! See [https://christoph-frenes.de/tubemercy/](https://christoph-frenes.de/tubemercy/). :smile:

### Phase 1: Research

My first thought was that [Yann van der Cruyssen](https://twitter.com/morusque), the owner of petittube.com, maybe has shared the code somewhere. But unfortunately this is not the case. Of course, this is not a problem and why should he? But for me it would have made things so much easier.

Okay, so let's have a look at Youtube and how we can use the search function or their API to fetch videos with almost no views. Well, while it is possible to sort search results by "View count" (see [Advanced Youtube Search](https://www.makeuseof.com/tag/search-youtube-pro-google-advanced-operators/)), we can only see the list sorted descending. There is no feature to flip that list around. Youtube also limits the amount of the results to 1000. This means with a very specific search term we might get some of the videos we want to find but as we want to get videos of any type this cannot be the solution. The API has the same restrictions by the way.

But how does petittube.com solve this? I mean it has to face the same limitations.

Having a look at the videos petittube.com is showing was an eye opener. All the videos seem to be:

- short (never longer than four minutes)
- relatively new (so far no video seems to be older than 2 months)
- almost unnoticed so far (of course, that's the whole premise of the project)

### Phase 2: My concept

Those details given, I came to the conclusion that I could do the following:

- create a cron job that fetches every video of youtube that was created in the last x minutes
- store the results into a database
- periodically remove videos from the database where:
    - the view count raises higher than a set limit (15 maybe)
    - the video was deleted or set to private
    - the video is not allowed to be embedded anymore
    - the video is older than a set limit (2 month maybe)
- create a dynamic website that fetches a random entry from the database and show it via the embedding code (iframe)

I do not say this is the way petittube.com does it, but it would work that way.

### Phase 3: Build it

I decided to use the following tool set:

- [Youtube Data API v3](https://developers.google.com/youtube/v3)
    - [Search](https://developers.google.com/youtube/v3/docs/search/list)
    - [Videos](https://developers.google.com/youtube/v3/docs/videos/list)
- PHP8 and Composer to operate the API and communicate with the DB
- [SleekDB](https://sleekdb.github.io): file based noSQL database
- [Skeleton](http://getskeleton.com): create a very small and lightweight web frontend

**Disclaimer:** It's not my goal to show best practice programming here, I just want to get things done. Security, style and antipatterns aren't the numer one priority here.

First of all you need a Google API key. See [Googles developer help](https://support.google.com/googleapi/answer/6158862?hl=en) for information on that. as a next step you have to keep in mind that even with the API key your requests to the API are limited. Google is working with a [quota model](https://developers.google.com/youtube/v3/getting-started#quota) here, which means every endpoint has a special quota value and if you reach your daily limit, you either have to pay for a higher limit or you need to wait until 00:00 PST. Your request counter will be reseted then. The quota for each request is also included in the documentation.

I want to split my code into three files, one that fetches the data from the API, one that updates the data from time to time and one responsible for the actual video showing.

#### Fetching

I'm using the Youtube Search API to get a list of the videos posted during the last hour but I try to filter the results as much as possible to keep a small database and reduce later update calls to the API to a minimum to stay below my daily limit. Unfortunately this API does not allow to ask for results with a viewCount smaller than x. So I fetch all videos regardless of their current viewCount. To avoid doubled values I call the script every hour with a cronjob. While storing the results to the database I also check, if I already have an entry with the specific video_id.

**Example API-URL:**

> ```shell
> https://www.googleapis.com/youtube/v3/search?key=$apiKey&maxResults=$maxResults&order=$sortBy&publishedAfter=$startAt&publishedBefore=$stopAt&type=video&videoDuration=short&videoEmbeddable=true&eventType=completed&safeSearch=strict
> ```

If the result set is larger than `maxResults` I currently ignore the rest to avoid addtional API calls and complexity.

#### Updating

The data update happens during another cronjob - currently also hourly and before the next fetch. This way I can wait almost an hour to see if some of the new videos have gained enough views to not be considered for our project anymore. I'm also checking for deleted videos and videos that are to old (currently 2 months). In both cases they will be deleted from the database.

There are a couple of tests more to keep the requests low. One additional option to also keep the requests low is to ask the API for the details of several videos in one request. One request can give you the details for up to 50 videos. To use this feature I'm building chunks of video_ids and send them separated by commas.

**Example API-URL:**

> ```shell
> https://www.googleapis.com/youtube/v3/videos?id=$bulkAsString&part=contentDetails,statistics,status&key=$apiKey
> ```

#### Viewing

I'm not a frontend developer. I just wanted to get things done and so I decided to use a small framework called Skeleton to structure my data output. I almost just used their example page and added the Youtube iframe with a random video id.

To really bring this to work it was also necessary to force the browser to not cache some of the contents because although the url of the iframe changed with every page load the browsers cached the scripts inside the loaded YouTube page. And so without a hard reload the same video would be loaded again and again.

**Example for disabling the browser cache:**

> Header unset ETag
> Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
> Header set Pragma "no-cache"
> Header set Expires "Wed, 12 Jan 1980 05:00:00 GMT"

**Notice:** Serverside Cache-Headers have a higher priority than those set in the meta tags of a HTML page. In my case the .htaccess file of a another directory interferred with my first attempts to disable the cache. I then switched to a designated `.htaccess`.

### Resume

So, that's it. That is how I think petittube.com works. Atleast it behaves like the original. So I'm happy for now but if you have any other or better ideas to accomplish this feel free to contact me and tell me. I'm interessted in your thought.

Right now, about one day after starting the fetching, the database is not very huge yet. I still often get videos shown I already saw before but I think this will get better while the database grows.

Thanks for reading. The code will be available soon via GitHub or gitlab.com.